import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class convert {
    final static String regularString="@#$&%*!^;,.";
    public static void convertImageToASSIC(BufferedImage bufferedImage){
        int imageWidth=bufferedImage.getWidth();
        int imageHeight=bufferedImage.getHeight();
        for(int coordinateX=0;coordinateX<imageHeight;coordinateX+=10){
            for(int coordinateY=0;coordinateY<imageWidth;coordinateY+=6){
                int orientRGB=bufferedImage.getRGB(coordinateY,coordinateX);
                int componentR=(orientRGB>>16)&0xff;
                int componentG=(orientRGB>>8)&0xff;
                int componentB=orientRGB&0xff;
                int pixelDegree= (int) (componentR*0.3+componentG*0.59+componentB*0.11);
                System.out.print(regularString.charAt(pixelDegree/24));
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        File imageFile=new File("E://常用文件/we.jpg");
        System.out.println(imageFile.getAbsoluteFile());
        try {
            BufferedImage bufferedImage= ImageIO.read(imageFile);
            convertImageToASSIC(bufferedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
